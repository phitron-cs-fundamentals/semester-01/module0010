#include<stdio.h>
int main (){
    int a[5];
    int sz=sizeof(a)/sizeof(int);
    printf("Total Size of Array: %d\n",sizeof(a));
    printf("Size of Array(Elements): %d\n",sz);

    char b[5];
    int siz = sizeof(b)/sizeof(char);
    printf("Per Char Size od Array: %d\n",sizeof(siz));
    printf("Total Size of Char Array: %d\n",sizeof(b));

    printf("Interger: %d\n",sizeof(int));
    printf("Character: %d\n",sizeof(char));
    printf("Long Integer: %d\n",sizeof(long long int));
    printf("Float: %d\n",sizeof(float));
    printf("Double: %d\n",sizeof(double));

    return 0;
}