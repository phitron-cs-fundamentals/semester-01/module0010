#include<stdio.h>
/**
 * 1st superpower - Initialization
 * 2nd superpower - Printf
 * 3rd superpower - Input
*/
int main (){
    char a[5]="mizan"; //will be print with garbage value
    printf("%s\n",a);

    char n[]="mizan"; //resolve above the problem 
    printf("%s\n",n);
    int sz=sizeof(n)/sizeof(char);
    printf("Total size of array: %d\n",sz);

    char b[6]="mizan";//1st superpower of Initialization of String
    printf("%s\n",b);

    char c[6]="mizan\0";//Here '\0' is null for end
    printf("%s\n",c);

    char m[24];
    scanf("%s",m);
    printf("Last: %s",m);
    return 0;
}