#include<stdio.h>
#include<string.h>
/**
 * Use gets() or fgets() instead of scanf()
 * standard fgets()
 * fgets() take enter as char
 * scanf and gets doesn't take
*/
int main (){
    char m[100];
    // gets(m);  //gets()
    fgets(m,23,stdin); //fgets() takes enter as char
    printf("%s",m);
    return 0;
}